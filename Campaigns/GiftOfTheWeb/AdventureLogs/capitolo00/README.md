# Gift of the Web - Adventure Logs - Capitolo 0

## [0] All’avventura

L’anno è il 1497 DR.

Forze della luce e dell’oscurità si stanno muovendo alcune alla luce del sole mentre altre agiscono nell’ombra…

L’avventura comincia nella citta’ di "[Trademeet](../../Worlds/Abeir-Toril/Faerun/Thethyr/Trademeet.md)" a est di "Murann"che è diventata di recente uno sblocco molto importante per gli scambi commerciali tra Thethyr and Amn.

Mercanti, mercenari, avventurieri, carpentieri, fabbri e anche maghi passano da tale città per gli ordini ricevuti o dai loro re o gilde, o divinità, …

Nel via vai generale in mezzo alla folla degli individui non sanno ancora che il destino a in serbo per loro qualcosa di molto diverso…



