# Gift of the Web


- [Gift of the Web](#gift-of-the-web)
    - [Regole ufficiali e della casa](#regole-ufficiali-e-della-casa)
    - [Adventure Logs](#adventure-logs)
  - [Introduction](#introduction)
    - [Prologo](#prologo)
    - [Il mondo in cui viviamo](#il-mondo-in-cui-viviamo)
  - [Artwork](#artwork)
    - [Request to Update Image Credits](#request-to-update-image-credits)
    - [Request to Remove Images](#request-to-remove-images)

### [Regole ufficiali e della casa](../../regole_di_gioco_utili/README.md)

### [Adventure Logs](./AdventureLogs/README.md)

## Introduction

Una delle tempeste più brutte che si siano mai viste su Toril ed insieme ad una nebbia impenetrabile proprio nel bel mezzo dell’oceano "Trackless sea" e all’improviso la calma piu’ completa…
In lontananza in mezzo all’oceano un portale di dimensioni mai viste ed aldilà di esso una terra incontaminata comparsa dal nulla…

### Prologo

In tutte e tre le nazioni costiere di Tethyr, Amn e Calimash non si parla d’altro, una corsa si è attivata nei porti di tutti la costa e in tutte le tre grandi nazioni non ci sono mercanti, carpentieri,  alchemisti e mercenari che non siano stati convocati dalle rispettive gilde e allo stesso tempo nessuno sa esattamente cosa stia avvenendo.

Niente è stato dichiarato ne dalla corona di Thethyr , ne dalla gilda dei mercanti di Amn, ne dal califfato di Callmash.

Un’enorme flotta viene costruita in fretta e furia ed enormi vascelli preparati a un viaggio molto lungo.

Per la prima volta da anni numerosi gruppi di maghi e stregoni di diverse fazioni e nazioni sono stati visti presenziare alle varie corti. Qualcosa di grosso e’ successo i riservisti sono stati richiamati e il reclutamento dei mercenari decuplicato.

Il re di Thehyr ha appena inaugurato la nuova nave ammiraglia della flotta chiamata "Conquista", la costruzione sua e della flotta a richiesto solo 1 anno ed è in pratica una citta’ gallegiante , (il solo fatto che gallegi è dovuto ad un’enorme rituale magico). Nessuno sa ancora come la corona abbia potuto finanziare e costruire il tutto in cosi poco tempo.

![Conquista](https://i.pinimg.com/564x/9b/eb/f0/9bebf0382ac288339164a6499506a3cc.jpg)

Un grande incontro delle tre nazioni sta per tenersi nelle prossime settimane nella ciità portuale di “Port Kir” a sud di “Mosstone” nella nazione di Thethyr e l’evento è nella bocca di tutti (nobili e non), si dice che un qualche ambasciatore di una terra lontana è arrivato in gran segreto, ma si sa il popolo ama chiaccherare…


### Il mondo in cui viviamo

Il mondo di forgotten realms.

## Artwork

All artwork and images found on this site are used for entertainment purposes only. 
No image was chosen as filler. 
I only choose images that I find beautiful, inspiring and that match what I imagine for the associated I spend 5-10 minutes researching each image used in attempt to provide credit with the following:

- Title
- Creator
- Link to creator's site

The artists and creators that own these works are incredibly talented and deserve our praise and credit.

### Request to Update Image Credits

Unfortunately, I'm not always able to track down that information successfully. If you find an image without a credit and you have the proper information, please open a issue on this project and I'll update credit appropriately.

### Request to Remove Images

If you own the rights to images found on this site and wish to see them removed, that is your right and I'll willingly comply. Please open a issue on this project and I'll attend to the request as soon as possible.
