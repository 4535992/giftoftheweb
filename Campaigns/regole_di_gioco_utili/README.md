# House Rules Compendium

## Official Rules

- [Azioni in combattimento](./official_rules/azioni_in_combattimento.md)
- [Cavalcature e Veicoli](./official_rules/cavalcature_e_veicoli.md)
- [Combattimenti SPeciali](./official_rules/combattimenti_speciali.md)
- [Combattimento](./official_rules/combattimento.md)
- [Condizioni e Status Alterati](./official_rules/condizioni_e_status_alterati.md)
- [Danno e Guarigione](./official_rules/danno_e_guarigione.md)
- [Effettuare un'attacco](./official_rules/effettuare_un_attacco.md)
- [Movimento e Posizione](./official_rules/movimento_e_posizione.md)
- [Regole relative all'avventure](./official_rules/regole_relative_all_avventura.md)
- [Regole relative alla magia](./official_rules/regole_relative_alla_magia.md)
- [Sollevamento e Trasporto](./official_rules/sollevamento_e_trasporto.md)
- [Talenti PHB](./official_rules/talenti_del_manuale_base_del_giocatore.md)

## House Rules (Basic)

- [Creare e modificare incantesimi](./house_rules_specific/creare_modificare_incantesimi.md)
- [Creare pggetti magici personalizzati](./house_rules_specific/creare_oggetti_magici_personalizzati.md)
- [Lesioni Persistenti](./house_rules_specific/lesioni_persistenti.md)
- [Lineage of Power Deities Faerun](./house_rules_specific/lesioni_persistenti.md)
- [Lineage of power generic](./official_rules/azioni_in_combattimento.md)
- [Oggetti e durabilita'](./house_rules_specific/oggetti_durabilita_e_soglia_danno.md)
- [Servizi Magici](./house_rules_specific/servizi_magici.md)

## House Rules 0 a 19(Compendium)

- [00 Generic Rules](./house_rules_compendium/00_house_rules_dm_only.md)
- ~~[01 Character Creation](./house_rules_compendium/01_house_rules_character_creation.md)~~
- [02 Skill/Check/Save](./house_rules_compendium/02_house_rules_skill_checks_save.md)
- [03 Feature Official](./house_rules_compendium/03_house_rules_feature_official.md)
- [04 Feature Homebrew](./house_rules_compendium/04_house_rules_feature_homebrew.md)
- ~~[05 Combat](./house_rules_compendium/05_house_rules_combat.md)~~
- ~~[06 Conditions Official](./house_rules_compendium/06_house_rules_conditions_official.md)~~
- ~~[07 Conditions Homebrew](./house_rules_compendium/07_house_rules_conditions_homebrew.md)~~
- ~~[08 XXX]()~~
- [09 Event Mix](./house_rules_compendium/09_house_rules_event_mix.md)
  - Distruggi una porta
  - Dipingi un capolovoro
  - Ammanetta qualcuno
  - ecc.  
- ~~[10 XXX]()~~
- ~~[11 Size Rules](./house_rules_compendium/11_house_rules_size_rules.md)~~
- [12 Reactions Homebrew](./house_rules_compendium/12_house_rules_reactions_homebrew.md)


## House Rules 20 in poi (Advanced) [DA COMPLETARE]

- [20 Expert Wear and Tear](./house_rules_compendium/20_house_rules_expert_wear_and_tear.md)

## Credits

- [99 Credits](./house_rules_compendium/99_house_rules_credits.md)